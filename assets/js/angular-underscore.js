
angular.module('underscore',[]);

angular.module('underscore').factory('_', function() {
  //Underscore must already be loaded on the page
  return window._;
});
