app.service('AuthService', [
  '$http',
  '$q',
  'SessionService',
 function($http,$q,SessionService){
  return {
    login:function (credentials) {
      var deffered = $q.defer();
      $http.post('/login.php', credentials,{
        headers:{
          "Content-Type":"application/x-www-form-urlencoded"
        }
      })
      .then(function (res) {
        console.log(res);
        if(res.data){
          var user = res.data;

          if(user != 0 && user != null && user != undefined){
            deffered.resolve(user);
          }else{
            deffered.reject(new Error('Authentication failed!'));
          }
        }else{
          deffered.reject(new Error('Authentication failed!'));
        }
      });

      return deffered.promise;
    },

    logout:function(){
      SessionService.destroy();
    },

    isAuthenticated:function () {
      // Checks whether or not someone has been logged in
      return !!SessionService.get('logged_in');
    },

    isAuthorized:function(){

    }

  };
}]);
