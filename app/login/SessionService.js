app.service('SessionService', ['$cookieStore',function ($cookieStore) {

  this.create = function (user) {
    var sessionId = (new Date()).getTime();
    $cookieStore.put('session_id',sessionId);
    $cookieStore.put('current_user',user);
    $cookieStore.put('logged_in','true');
  };

  this.destroy = function() {
    $cookieStore.put('session_id',null);
    $cookieStore.put('current_user',null);
    $cookieStore.put('logged_in',null);
  };

  this.update = function(user){
    $cookieStore.put('current_user',user);
  };

  this.get = function(key){
    return $cookieStore.get(key);
  };


}]);
