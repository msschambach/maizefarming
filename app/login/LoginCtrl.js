app.controller('LoginCtrl',[
  '$scope',
  '$rootScope',
  '$state',
  'AUTH_EVENTS',
  'AuthService',
  'SessionService',
  function($scope,$rootScope,$state,AUTH_EVENTS,AuthService,SessionService){

    // TODO: You need to change $scope.alerts from an array to an object, it's not the best implementation.
    $scope.alerts = [
      {
        type:'danger',
        when:'notLoggedIn',
        msg:'You need to login!'
      },
      {
        type:'danger',
        when:'loginFailed',
        msg:'Login failed! Wrong username or password!'
      }
    ];




    $scope.closeAlerts=function(index){
      $scope.alerts.splice(index, 1);
    };


    $scope.loginRequired = $state.params.loginRequired;


    $scope.credentials = {
      email:'',
      password:''
    };


    $scope.login = function(credentials){
      console.log("Logging in...");
      AuthService.login({
        email:credentials.email.trim(),
        password:credentials.password.trim()
      }).then(function (user) {

        SessionService.create(user);
        console.log('login success!');
        $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
      }, function (error) {
        $rootScope.$broadcast(AUTH_EVENTS.loginFailed, error);
        console.log(error);
      });
    };
}]);
