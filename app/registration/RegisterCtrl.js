app.controller('RegisterCtrl',[
  '$scope',
  '$state',
  '_',
  'RegisterService',
  function($scope,$state,_,RegisterService){
    $scope.register = function(form_data){
      console.log("Registering...");
      form_data.format_mobile_number();
      console.log(form_data);
    };

    $scope.form_data = {
      raw_mobile_number:null,
      format_mobile_number:function(){
        this.mobile_number = "+254" + String(this.raw_mobile_number);
      }
    };
}]);
