/*Define the application*/

var app = angular.module('maizefarming',[
  'underscore',
  'ngCookies',
  'ngTouch',
  'ui.router',
  'ui.bootstrap'
  ]);


  /*Configure things*/

    /*Set Up Constants*/
    app.constant('APP_TITLE','Maize Farming Companion');
    app.constant('APP_VERSION','0.0.0');
    app.constant('AUTH_EVENTS', {
      loginSuccess: 'auth-login-success',
      loginFailed: 'auth-login-failed',
      logoutSuccess: 'auth-logout-success',
      sessionTimeout: 'auth-session-timeout',
      notAuthenticated: 'auth-not-authenticated',
      notAuthorized: 'auth-not-authorized'
    });
    app.constant('USER_ROLES', {
      all: '*',
      admin: 'admin',
      editor: 'editor',
      guest: 'guest'
    });
