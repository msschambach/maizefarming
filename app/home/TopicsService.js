app.service('TopicsService', ['$http', '$q', function($http, $q){
    var deffered = $q.defer();

    this.getTopics = function(){
      return [
        {
          title:"Preparation",
          content:"Information about how to prepare the land",
          accordionStatus:false
        },
        {
          title:"Planting",
          content:"Information about planting maize",
          accordionStatus:false
        },
        {
          title:"Early Growth",
          content:"Information about what to do in the early growth ",
          accordionStatus:false
        },
        {
          title:"Health",
          content:"Information about how to monitor the health of the maize",
          accordionStatus:false
        }
      ];
    };
}]);
