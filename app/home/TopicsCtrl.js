app.controller('TopicsCtrl',[
  '$scope',
  '$state',
  '_',
  'TopicsService',
  function($scope,$state,_,TopicsService){
    $scope.accordion_status = {
      preparation:false,
      planting:false,
      early_growth:false,
      health:false,
    };

    $scope.topics = TopicsService.getTopics();

}]);
