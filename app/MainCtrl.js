app.controller('MainCtrl',[
  '$scope',
  '$rootScope',
  '$state',
  'AuthService',
  'AUTH_EVENTS',
  function($scope,$rootScope,$state,AuthService,AUTH_EVENTS){
    $rootScope.$on(AUTH_EVENTS.loginSuccess,function(event){
      $scope.loggedIn = true;
    });

    $rootScope.$on(AUTH_EVENTS.logoutSuccess,function(event){
      $scope.loggedIn = false;
    });

    $scope.logout = function(){
      AuthService.logout();
      $rootScope.$broadcast(AUTH_EVENTS.logoutSuccess);
    };

}]);
