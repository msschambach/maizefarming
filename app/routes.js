
/* Routes for the application*/
app.config(['$stateProvider','$urlRouterProvider','USER_ROLES',function($stateProvider, $urlRouterProvider, USER_ROLES) {

    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('home',{
          url:'/',
          views:{
            'header':{
              templateUrl:'app/home/header-view.html'
            },
            'content':{
              templateUrl:'app/home/content-view.html'
            },
            'footer':{
              templateUrl:'app/home/footer-view.html'
            }
          },
          data: {
            requireLogin:false,
            authorizedRoles: [USER_ROLES.all],
            pageTitle:'Home'
          }
        })
        .state('home.dashboard',{
          url:'dashboard',
          views:{
            'dashboard@home':{
              templateUrl:'app/home/dashboard-view.html',
              controller:'DashboardCtrl'
            }
          },
          data: {
            requireLogin:true,
            pageTitle:'Dashboard'
          }
        })
        .state('home.topics',{
          url:'topics/',
          views:{
            'topics@home':{
              templateUrl:'app/home/topics-view.html',
              controller:'TopicsCtrl'
            }
          },
          data: {
            requireLogin:true,
            pageTitle:'Topics'
          }
        })
        .state('home.login', {
            url: 'login/:loginRequired',
            views:{
              'content@':{
                templateUrl:'app/login/login-view.html',
                controller:'LoginCtrl'
              }
            },
            data: {
              requireLogin:false,
              pageTitle:'Login'
            }
        })
        .state('home.register',{
          url:'register',
          views:{
            'content@':{
              templateUrl:'app/registration/register-view.html',
              controller:'RegisterCtrl'
            }
          },
          data: {
            requireLogin:false,
            pageTitle:'Register'
          }
        });

  }])
  .run([
    '$rootScope',
    '$state',
    'AuthService',
    'AUTH_EVENTS',
    'SessionService',
    function($rootScope, $state, AuthService, AUTH_EVENTS,SessionService){
    $rootScope.$state = $state;
    /*This part is a mess TODO:Fix it.*/
    // $rootScope.$on('$stateChangeStart', function (event,toState,toParams,fromState,fromParams) {
    //   var authorizedRoles = toState.data.authorizedRoles;
    //   var requireLogin = toState.data.requireLogin;
    //
    //   console.log(toParams);
    //   // Update session data
    //   $rootScope.loggedIn = AuthService.isAuthenticated();
    //   $rootScope.user = SessionService.get('current_user');
    //
    //   // Check if the route requires login
    //   if (requireLogin) {
    //
    //     // Check if user has logged in
    //     if(!AuthService.isAuthenticated()){
    //       // User not logged in
    //       event.preventDefault();// prevent loading the route content
    //
    //       // Take user to login page.
    //       $state.go('home.login',{loginRequired:true},{inherit:false});
    //     }
    //   }
    // });
    //
    // $rootScope.$on(AUTH_EVENTS.loginSuccess,function(event){
    //   // Since it's a success, go home!
    //   $state.go('home');
    // });
    //
    // $rootScope.$on(AUTH_EVENTS.logoutSuccess,function(event){
    //   // Since logged out successfully, take to login page
    //   $state.go('home.login');
    // });
    //
    // $rootScope.$on(AUTH_EVENTS.loginFailed,function(event, error){
    //   // Login failed
    //   $state.go('home.login',{
    //     loginRequired:true
    //   });
    // });

  }]);
